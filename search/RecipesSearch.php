<?php
namespace app\search;

use yii\base\Model;
use app\models\Integritty;
use app\models\IntegrittyInRecipe;
use app\models\Recipes;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\db\Query;

class RecipesSearch extends Model
{
    public $integritty = [];
   
    public function rules()
    {
        return [
            ['integritty', 'each', 'rule' => ['integer']],
            ['integritty', 'validateIntegritty'],
        ];
    }
    
    public function validateIntegritty()
    {   
        $count = count($this->integritty);
        if (!$count || $count > 5 || $count < 2) { 
            $this->addError('integritty', "Выберите от двух до пяти ингридиентов");
        }
    }

    public function integrittyList()
    {
        return ArrayHelper::map(Integritty::find()->orderBy('name')->asArray()->all(), 'id', 'name');
    }
    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search(array $params)
    {
       $query = $this->find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }
    
        $query
             ->andFilterWhere(['in','integritty_id', $this->integritty]);

        return $dataProvider;
    }

    protected function find() {

        $query = IntegrittyInRecipe::find()
            ->select(['recipe_id', 'integritty_id', 'COUNT(recipe_id) AS countRecipe'])
            ->groupBy(['recipe_id'])
            ->orderBy(['countRecipe' => SORT_DESC])
            ->having(['>', 'countRecipe', 1]);
        
        $all = $query->all();
        $array = [];
        
        foreach ($all as $value) {
           if(!Recipes::findOne($value->recipe_id)->hideIntegrittyInRecipes) {
                $array[] = $value->recipe_id;
           }
       }

        $query->where(['recipe_id'=>$array]);   
        return $query;     
    }
     
}
