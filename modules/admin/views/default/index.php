<?php
use yii\helpers\Html;

$this->title = 'Добро пожаловать в панель управлениия!';
?>
 <div class="jumbotron">
  <center><h1><?= Html::encode($this->title) ?></h1></center>
<div class="row">  
  <div class="col-md-6">
     <?= Html::a('Ингредиенты',['integritty/index'], ['class'=>"btn btn-primary btn-lg btn-block"])?>
</div>
 <div class="col-md-6">
   <?= Html::a('Блюда',['recipes/index'], ['class'=>"btn btn-success btn-lg btn-block"])?>
        

</div>

</div>
</div>