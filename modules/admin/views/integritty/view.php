<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\data\ActiveDataProvider;
use app\modules\admin\helpers\IntegrittyInRecipeHelper;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Integritty */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Ингредиенты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="integritty-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a("Удалить", ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
        ],
    ]) ?>
 <h4>Список блюд из данного ингредиента</h4>
<div>
      <?= GridView::widget([
         'dataProvider' => new ActiveDataProvider(['query' => $model->getIntegrittyInRecipes()]),
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'recipe.name',
            ['attribute'=>'status',
             'format'=>"raw",
             'value' => function($model) {
                 return IntegrittyInRecipeHelper::statusLabel($model->status);
            }],
            ['class' => 'yii\grid\ActionColumn',
             'template' => '{link} {delete}',
              'buttons' => [
               "delete" => function ($url,$model,$key) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', ["integritty-in-recipe/delete", 'id' => $model->id],['data-method' =>"post"]);},
            
                "link" => function ($url,$model,$key) {
                    return Html::a(IntegrittyInRecipeHelper::statusIcon($model->status), [IntegrittyInRecipeHelper::statusLink($model->status), 'id' => $model->id],['data-method' =>"post"]);},
                ],
            ],
        ],
    ]); ?>
</div>

</div>
