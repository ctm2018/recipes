<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\data\ActiveDataProvider;
use app\modules\admin\helpers\IntegrittyInRecipeHelper;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Recipes */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Блюда', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="recipes-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Обновить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'вы уверены, что хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            ['label' => "Cтатус блюда",
             'format'=>"raw",
             'value' =>  IntegrittyInRecipeHelper::statusLabel($model->statusRecipe)],
            ['label' => "Активные игредиенты",
             'value' => $model->stringCountIntegrittyInRecipes],
        ],
    ]) ?>

</div>
<h4>Ингредиенты</h4>
<div>
      <?= GridView::widget([
         'dataProvider' => new ActiveDataProvider(['query' => $model->getIntegrittyInRecipes()]),
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'integritty.name',
            ['attribute'=>'status',
             'format'=>"raw",
             'value' => function($model) {
                  return IntegrittyInRecipeHelper::statusLabel($model->status);
            }],
            ['class' => 'yii\grid\ActionColumn',
             'template' => '{delete} {link}',
              'buttons' => [
               "delete" => function ($url,$model,$key) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', ["integritty-in-recipe/delete", 'id' => $model->id],['data-method' =>"post"]);},
                "link" => function ($url,$model,$key) {
                return Html::a(IntegrittyInRecipeHelper::statusIcon($model->status), [IntegrittyInRecipeHelper::statusLink($model->status), 'id' => $model->id],['data-method' =>"post"]);},
                ],
            ],
        ],
    ]); ?>
</div>
