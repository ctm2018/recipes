<?php

namespace app\modules\admin\helpers;

use app\models\IntegrittyInRecipe;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class IntegrittyInRecipeHelper
{
    public static function statusList()
    {
        return [
            IntegrittyInRecipe::DRAFT => 'Cкрытый',
            IntegrittyInRecipe::ACTIVE => 'Активный',
        ];
    }

    public static function statusName($status)
    {
        return ArrayHelper::getValue(self::statusList(), $status);
    }

    public static function statusLink($status)
    {
        switch ($status) {
            case IntegrittyInRecipe::DRAFT:
                $href = 'integritty-in-recipe/activate';
                break;
            case IntegrittyInRecipe::ACTIVE:
                $href = 'integritty-in-recipe/draft';
                break;
            default:
                $href = '#';
        }
        return $href;
    }

     public static function statusIcon($status)
    {
        switch ($status) {
            case IntegrittyInRecipe::DRAFT:
                $icon = '<span class="glyphicon glyphicon-ok"></span>';
                break;
            case IntegrittyInRecipe::ACTIVE:
                $icon = '<span class="glyphicon glyphicon-remove"></span>';
                break;
            default:
                $icon = '';
        }

        return $icon;
    }

    public static function statusLabel($status)
    {
        switch ($status) {
            case IntegrittyInRecipe::DRAFT:
                $class = 'label label-danger';
                break;
            case IntegrittyInRecipe::ACTIVE:
                $class = 'label label-success';
                break;
            default:
                $class = 'label label-default';
        }

        return Html::tag('span',self::statusName($status), ["class"=>$class]);
    }
}