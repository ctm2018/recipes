<?php

namespace app\modules\admin\forms;

use app\models\Integritty;
use app\models\Recipes;
use yii\base\Model;
use yii\helpers\ArrayHelper;

class RecipesForm extends Model
{  
    public $name;
    public $integritty = [];
	
	public function __construct(Recipes $recipe = null, $config = [])
    {
       if ($recipe) {
            $this->name = $recipe->name;
            $this->integritty = ArrayHelper::getColumn($recipe->integrittyInRecipes, 'integritty_id');
        }
    }
   
    public function rules()
    {
        return [
            [['name','integritty'],'required'],
            [['name'], 'string'],
            ['name', 'unique', 'targetClass' => '\app\models\Recipes'],
            ['integritty', 'each', 'rule' => ['integer']],
            ['integritty', 'validateIntegritty'],
        ];
    }
    
    public function validateIntegritty()
    {   
        $count = count($this->integritty);
        if ($count > 5 || $count < 2) {
            $this->addError('integritty', 'От двух до пяти');
        }
    }

    public function attributeLabels()
    {
        return [
           'integritty'=> "Ингредиенты",
            'name' => 'Наименование блюда',
        ];
    }


    public function integrittyList()
    {
        return ArrayHelper::map(Integritty::find()->orderBy('name')->asArray()->all(), 'id', 'name');
    }

}
