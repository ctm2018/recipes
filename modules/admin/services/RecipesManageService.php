<?php

namespace app\modules\admin\services;

use app\models\Recipes;
use app\modules\admin\forms\RecipesForm;
use app\modules\admin\repositories\IntegrittyRepository;
use app\modules\admin\repositories\RecipesRepository;

class RecipesManageService
{
    private $recipe;
    private $integritty;

    public function __construct(RecipesRepository $recipe, IntegrittyRepository $integritty) 
    {   
        $this->integritty = $integritty;
        $this->recipe= $recipe;
    }

    public function create(RecipesForm $form)
    {
        $recipe = Recipes::create($form->name);
        foreach ($form->integritty as $integrittyId) {
            $integritty = $this->integritty ->get($integrittyId);
            $recipe->addIntegrittyInRecipes($integritty->id);
        }

        $this->recipe->save($recipe);
        return $recipe;
    }

    public function edit($id, RecipesForm $form)
    {
        $recipe = $this->recipe->get($id);
        $recipe->edit($form->name);
        $recipe->revokeIntegrittyInRecipes();
        $this->recipe->save($recipe);

        foreach ($form->integritty as $integrittyId) {
            $integritty = $this->integritty ->get($integrittyId);
            $recipe->addIntegrittyInRecipes($integritty->id);
        }
         $this->recipe->save($recipe);
    }

    public function remove($id)
    {
        $recipe = $this->recipe->get($id);
        $this->recipe->remove($recipe);
    }
}