<?php

namespace app\modules\admin\services;

use app\modules\admin\repositories\IntegrittyInRecipeRepository;


class IntegrittyInRecipeManageService
{
    private $integrittyInRecipe;

    public function __construct(IntegrittyInRecipeRepository $integrittyInRecipe) 
    {   
        $this->integrittyInRecipe=$integrittyInRecipe;
    }

    public function activate($id)
    {
        $integrittyInRecipe = $this->integrittyInRecipe->get($id);
        $integrittyInRecipe->activate();
        $this->integrittyInRecipe->save($integrittyInRecipe);
    }

    public function draft($id)
    {
        $integrittyInRecipe = $this->integrittyInRecipe->get($id);
        $integrittyInRecipe->draft();
        $this->integrittyInRecipe->save($integrittyInRecipe);
    }

    public function remove($id)
    {
        $integrittyInRecipe = $this->integrittyInRecipe->get($id);
        $this->integrittyInRecipe->remove($integrittyInRecipe);
    }
}