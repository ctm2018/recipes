<?php

namespace app\modules\admin\repositories;

use app\models\Integritty;

class IntegrittyRepository
{
    public function get($id)
    {
        if (!$integritty = Integritty::findOne($id)) {
            throw new NotFoundException('Integritty is not found.');
        }
        return $integritty;
    }

    public function save(Integritty $integritty)
    {
        if (!$integritty->save()) {
            throw new \RuntimeException('Saving error.');
        }
    }

    public function remove(Integritty $integritty)
    {
        if (!$integritty->delete()) {
            throw new \RuntimeException('Removing error.');
        }
    }
}