<?php

namespace app\modules\admin\repositories;

use app\models\IntegrittyInRecipe;

class IntegrittyInRecipeRepository
{
    public function get($id)
    {
        if (!$integrittyinrecipe = IntegrittyInRecipe::findOne($id)) {
            throw new NotFoundException('IntegrittyInRecipe is not found.');
        }
        return $integrittyinrecipe;
    }

    public function save(IntegrittyInRecipe $integrittyinrecipe)
    {
        if (!$integrittyinrecipe->save()) {
            throw new \RuntimeException('Saving error.');
        }
    }

    public function remove(IntegrittyInRecipe $integrittyinrecipe)
    {
        if (!$integrittyinrecipe->delete()) {
            throw new \RuntimeException('Removing error.');
        }
    }
}