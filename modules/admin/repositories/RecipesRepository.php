<?php

namespace app\modules\admin\repositories;

use app\models\Recipes;

class RecipesRepository
{
    public function get($id)
    {
        if (!$recipe = Recipes::findOne($id)) {
            throw new NotFoundException('Recipe is not found.');
        }
        return $recipe;
    }

    public function save(Recipes $recipe)
    {
        if (!$recipe->save()) {
            throw new \RuntimeException('Saving error.');
        }
    }

    public function remove(Recipes $recipe)
    {
        if (!$recipe->delete()) {
            throw new \RuntimeException('Removing error.');
        }
    }
}