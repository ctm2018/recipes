<?php

use yii\helpers\Html;
use yii\widgets\ListView;



$this->title = 'Главная';
?>
 <h1><?= Html::encode($this->title) ?></h1>
<div class="row">

  <div class="col-md-4">
  
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

</div>

 <div class="col-md-8">

        

    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        
        'itemView' => function ($model, $key, $index, $widget) {
        
           return $model->recipe->data;
                   },
    ]) ?>

        

</div>

</div>