<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get', 
    ]); ?>

   
    
        <?= $form->field($model, 'integritty')->label("Ингредиенты")->checkboxList($model->integrittyList()) ?>

  
        <?= Html::submitButton('Найти блюдо', ['class' => 'clearfix btn btn-success', "style"=>"width:100%"]) ?> 
    

    <?php ActiveForm::end(); ?>

