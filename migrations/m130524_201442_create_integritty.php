<?php

use yii\db\Migration;

/**
 * Handles the creation of table `integritty`.
 */
class m130524_201442_create_integritty extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable('{{%integritty}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()
             ], $tableOptions);

         $this->insert('{{%integritty}}', [
            'id' => 1,
            'name' => 'Оливковое масло'
                ]);
  

        $this->insert('{{%integritty}}', [
            'id' => 2,
            'name' => 'Cливочное масло',
                ]);
 
         $this->insert('{{%integritty}}', [
            'id' => 3,
            'name' => 'Молоко',
                ]);


          $this->insert('{{%integritty}}', [
            'id' => 4,
            'name' => 'Яйцо'
                ]);

          $this->insert('{{%integritty}}', [
            'id' => 5,
            'name' => 'Пшеничная мука',
                ]);

           $this->insert('{{%integritty}}', [
            'id' => 6,
            'name' => 'Вода',
                ]);

        $this->insert('{{%integritty}}', [
            'id' => 7,
            'name' => 'Дрожжи',
                ]);
    
        $this->insert('{{%integritty}}', [
            'id' => 8,
            'name' => 'Сода',
                ]);

        $this->insert('{{%integritty}}', [
            'id' => 9,
            'name' => 'Моцарелла',
                ]);

       $this->insert('{{%integritty}}', [
            'id' => 10,
            'name' => 'Сыр',
                ]);
    }
    
    public function down()
    {
        $this->dropTable('{{%integritty}');
    }
}
