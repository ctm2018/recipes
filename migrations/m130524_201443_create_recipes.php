<?php

use yii\db\Migration;

/**
 * Handles the creation of table `recipes`.
 */
class m130524_201443_create_recipes extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable('{{%recipes}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
             ], $tableOptions);
        
         $this->insert('{{%recipes}}', [
            'id' => 1,
            'name' => 'Оладьи'
                ]);
  

        $this->insert('{{%recipes}}', [
            'id' => 2,
            'name' => 'Блины',
                ]);
 
         $this->insert('{{%recipes}}', [
            'id' => 3,
            'name' => 'Пицца',
                ]);
         
          $this->insert('{{%recipes}}', [
            'id' => 4,
            'name' => 'Хачапури'
                ]);

          $this->insert('{{%recipes}}', [
            'id' => 5,
            'name' => 'Фокачча',
                ]);

        $this->insert('{{%recipes}}', [
            'id' => 6,
            'name' => "Булочки",
                ]);
    }
    
    public function down()
    {
        $this->dropTable('{{%recipes}');
    }
}
