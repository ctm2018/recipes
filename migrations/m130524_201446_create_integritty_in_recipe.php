<?php

use yii\db\Migration;

/**
 * Handles the creation of table `integritty`.
 */
class m130524_201446_create_integritty_in_recipe extends Migration
{
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable('{{integritty_in_recipe}}', [
            'id' => $this->primaryKey(),
            'recipe_id' => $this->integer(11)->notNull(),
            'integritty_id' => $this->integer(11)->notNull(),
            'status' => $this->integer(1)->notNull(),
             ], $tableOptions); 

        $this->createIndex('{{%idx-integritty}}', '{{%integritty_in_recipe}}', 'integritty_id');
        $this->addForeignKey('{{%fk-idx-integritty}}', '{{%integritty_in_recipe}}', 'integritty_id', '{{%integritty}}', 'id',  'CASCADE', 'RESTRICT');
        
        $this->createIndex('{{%idx-recipes}}', '{{%integritty_in_recipe}}', 'recipe_id');
        $this->addForeignKey('{{%fk-idx-recipes}}', '{{%integritty_in_recipe}}', 'recipe_id', '{{%recipes}}', 'id',  'CASCADE', 'RESTRICT');

        $this->insert('{{%integritty_in_recipe}}', [
            'id' => 1,
            'recipe_id' => 1,
            'integritty_id' => 3,
            'status' => 1,
        ]);

       $this->insert('{{%integritty_in_recipe}}', [
            'id' => 2,
            'recipe_id' => 1,
            'integritty_id' => 4,
            'status' => 1,
        ]);

       $this->insert('{{%integritty_in_recipe}}', [
            'id' => 3,
            'recipe_id' => 1,
            'integritty_id' => 5,
            'status' => 1,
        ]);

       $this->insert('{{%integritty_in_recipe}}', [
            'id' => 4,
            'recipe_id' => 1,
            'integritty_id' => 7,
            'status' => 1,
        ]);

       //

       
        $this->insert('{{%integritty_in_recipe}}', [
            'id' => 5,
            'recipe_id' => 2,
            'integritty_id' => 3,
            'status' => 1,
        ]);

       $this->insert('{{%integritty_in_recipe}}', [
            'id' => 6,
            'recipe_id' => 2,
            'integritty_id' => 4,
            'status' => 1,
        ]);

       $this->insert('{{%integritty_in_recipe}}', [
            'id' => 7,
            'recipe_id' => 2,
            'integritty_id' => 5,
            'status' => 1,
        ]);

       //


        $this->insert('{{%integritty_in_recipe}}', [
            'id' => 8,
            'recipe_id' => 3,
            'integritty_id' => 6,
            'status' => 1,
        ]);

         $this->insert('{{%integritty_in_recipe}}', [
            'id' => 9,
            'recipe_id' => 3,
            'integritty_id' => 1,
            'status' => 1,
        ]);

       $this->insert('{{%integritty_in_recipe}}', [
            'id' => 10,
            'recipe_id' => 3,
            'integritty_id' => 9,
            'status' => 1,
        ]);

       $this->insert('{{%integritty_in_recipe}}', [
            'id' => 11,
            'recipe_id' => 3,
            'integritty_id' => 5,
            'status' => 1,
        ]);

       //

        $this->insert('{{%integritty_in_recipe}}', [
            'id' => 12,
            'recipe_id' => 4,
            'integritty_id' => 4,
            'status' => 1,
        ]);

       $this->insert('{{%integritty_in_recipe}}', [
            'id' => 13,
            'recipe_id' => 4,
            'integritty_id' => 5,
            'status' => 1,
        ]);

       $this->insert('{{%integritty_in_recipe}}', [
            'id' => 14,
            'recipe_id' => 4,
            'integritty_id' => 6,
            'status' => 1,
        ]);

       $this->insert('{{%integritty_in_recipe}}', [
            'id' => 15,
            'recipe_id' => 4,
            'integritty_id' => 7,
            'status' => 1,
        ]);

       $this->insert('{{%integritty_in_recipe}}', [
            'id' => 16,
            'recipe_id' => 4,
            'integritty_id' => 10,
            'status' => 1,
        ]);
    //

       $this->insert('{{%integritty_in_recipe}}', [
            'id' => 17,
            'recipe_id' => 5,
            'integritty_id' => 1,
            'status' => 1,
        ]);

       $this->insert('{{%integritty_in_recipe}}', [
            'id' => 18,
            'recipe_id' => 5,
            'integritty_id' => 6,
            'status' => 1,
        ]);

       $this->insert('{{%integritty_in_recipe}}', [
            'id' => 19,
            'recipe_id' => 5,
            'integritty_id' => 5,
            'status' => 1,
        ]);

        $this->insert('{{%integritty_in_recipe}}', [
            'id' => 20,
            'recipe_id' => 6,
            'integritty_id' => 2,
            'status' => 1,
        ]);

       $this->insert('{{%integritty_in_recipe}}', [
            'id' => 21,
            'recipe_id' => 6,
            'integritty_id' => 7,
            'status' => 1,
        ]);

       $this->insert('{{%integritty_in_recipe}}', [
            'id' => 22,
            'recipe_id' => 6,
            'integritty_id' => 5,
            'status' => 1,
        ]);


       $this->insert('{{%integritty_in_recipe}}', [
            'id' => 23,
            'recipe_id' => 6,
            'integritty_id' => 8,
            'status' => 1,
        ]);


       $this->insert('{{%integritty_in_recipe}}', [
            'id' => 24,
            'recipe_id' => 6,
            'integritty_id' => 3,
            'status' => 1,
        ]);
    }



    public function down()
    {
        $this->dropTable('{{%integritty_in_recipe}');
    }
}
