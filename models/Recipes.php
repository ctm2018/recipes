<?php

namespace app\models;

use Yii;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;


/**
 * This is the model class for table "recipes".
 *
 * @property int $id
 * @property string $name
 *
 * @property IntegrittyInRecipe[] $integrittyInRecipes
 */
class Recipes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'recipes';
    }

    public static function create($name)
    {
        $recipe = new static();
        $recipe->name = $name;
        return $recipe;
    }

    public function edit($name)
    {
        $this->name = $name;
    }


    public function addIntegrittyInRecipes($integritty_id)
    { 
        $integrittyes = $this->integrittyInRecipes;
        foreach ($integrittyes as $integritty) {
            if ($integritty->isForIntegritty($integritty_id)) {
                return;
            }
        }
        $integrittyes[] = IntegrittyInRecipe::create($integritty_id);
        $this->integrittyInRecipes = $integrittyes;
    }

    public function revokeIntegrittyInRecipes()
    {
        $this->integrittyInRecipes = [];
    }


    /**
     * @return \yii\db\ActiveQuery
     */

    public function getIntegrittyInRecipes()
    {
        return $this->hasMany(IntegrittyInRecipe::className(), ['recipe_id' => 'id']);
    }

    public function getCountIntegrittyInRecipes()
    {
        return $this->getIntegrittyInRecipes()->count();
    }

    public function getHideIntegrittyInRecipes()
    {
        return $this->getIntegrittyInRecipes()->where(['status'=>IntegrittyInRecipe::DRAFT]);
    }



    public function getCountActiveIntegrittyInRecipes()
    {
        return $this->getIntegrittyInRecipes()->where(['status'=>IntegrittyInRecipe::ACTIVE])->count();
    }

    public function getStringCountIntegrittyInRecipes() {

         return $this->countActiveIntegrittyInRecipes." из ".$this->countIntegrittyInRecipes;
    }

    public function isBoolCountIntegrittyInRecipes() {

         return $this->countIntegrittyInRecipes == $this->countActiveIntegrittyInRecipes;
    }

     public function getData() {
          if (!$this->hideIntegrittyInRecipes) {
           $data = '<h4>'.$this->name.'</h4>';
           $data .= '<p>Ингридиенты:';
           foreach ($this->integrittyInRecipes as $integritty) {
              $data.=  mb_strtolower($integritty->integritty->name)." ";
           }
              $data .= '</p>';
           }

             return $data;
         }



    public function getStatusRecipe() {
          if (!$this->hideIntegrittyInRecipes) {
              $status = IntegrittyInRecipe::ACTIVE;
          } else {
              $status = IntegrittyInRecipe::DRAFT;
          }
          return $status;
    }


     public function behaviors() {
        return [
            [
                'class' => SaveRelationsBehavior::className(),
                'relations' => ['integrittyInRecipes'],
            ],
        ];
    }
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование блюда',
        ];
    }

}
