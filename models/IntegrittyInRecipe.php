<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "integritty_in_recipe".
 *
 * @property int $id
 * @property int $recipe_id
 * @property int $integritty_id
 *
 * @property Integritty $integritty
 * @property Recipes $recipe
 */
class IntegrittyInRecipe extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    
    const ACTIVE = 1;
    const DRAFT = 0;

    public static function tableName()
    {
        return 'integritty_in_recipe';
    }

    public static function create($integritty_id)
    {
        $i_p = new static();
        $i_p->integritty_id =$integritty_id;
        $i_p->status = self::ACTIVE;
        return $i_p;
    }

    public function isForIntegritty($integritty_id)
    {
        return $this->integritty_id == $integritty_id;
    }

    public function activate()
    {
        if ($this->isActive()) {
            throw new \DomainException('IntegrittyInRecipe is already active.');
        }
        $this->status = self::ACTIVE;
    }

    public function draft()
    {
        if ($this->isDraft()) {
            throw new \DomainException('IntegrittyInRecipe is already draft.');
        }
        $this->status = self::DRAFT;
    }

    public function isActive()
    {
        return $this->status == self::ACTIVE;
    }

    public function isDraft()
    {
        return $this->status == self::DRAFT;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIntegritty()
    {
        return $this->hasOne(Integritty::className(), ['id' => 'integritty_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecipe()
    {
        return $this->hasOne(Recipes::className(), ['id' => 'recipe_id']);
    }

    public function attributeLabels()
    {
        return [
            'status' => 'Статус',
        ];
    }

}
