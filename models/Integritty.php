<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "integritty".
 *
 * @property int $id
 * @property string $name
 * @property int $status
 *
 * @property IntegrittyInRecipe[] $integrittyInRecipes
 */
class Integritty extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'integritty';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            ['name', 'unique', 'targetClass' => '\app\models\Integritty'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование ингредиента',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIntegrittyInRecipes()
    {
        return $this->hasMany(IntegrittyInRecipe::className(), ['integritty_id' => 'id']);
    }
}
